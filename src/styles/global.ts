import { createGlobalStyle } from 'styled-components';

export default createGlobalStyle`
  * {
    margin: 0;
    padding: 0;
    outline: 0;
    box-sizing: border-box;
  }

  html {
    scroll-behavior: smooth;
  }

  html, body, #root {
    height: 100%;
  }

  body {
    background-color: #fff;
    -webkit-font-smoothing: antialiased;
  }

  body, input, button, a {
    font-family: 'Open Sans', sans-serif;
    font-size: 16px;
    color: #000;
  }

  h1, h2, h3, h4, h5, h6 {
    font-weight: bold;
  }

  button {
    cursor: pointer;
  }

  a {
    text-decoration: none;
  }
`;
