import { DefaultTheme } from 'styled-components';

export const theme: DefaultTheme = {
  colors: {
    dark: '#010101',
    light: '#F1F1F1',
    primary: '#FCDB00',
  },
};
