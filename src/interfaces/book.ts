export type Book = {
  id: string;
  title: string;
  description: string;
  thumbnail: string;
  publishedDate: string;
};
