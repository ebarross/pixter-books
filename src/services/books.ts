import HttpClient from '../http/client';
import { HttpStatusCode } from '../interfaces/http';
import { Book } from '../interfaces/book';
import { UnexpectedError } from '../errors/unexpectedError';

export default {
  getAll: async (): Promise<Book[]> => {
    const response = await HttpClient.request({
      url: '/volumes',
      method: 'GET',
      queries: [
        {
          name: 'q',
          value: 'STAR WARS',
        },
        {
          name: 'maxResults',
          value: '8',
        },
      ],
    });

    const books: Book[] = response.body.items.map((item: any) => {
      const { title, description, publishedDate, imageLinks } = item.volumeInfo;
      return {
        title,
        description,
        publishedDate,
        id: item.id,
        thumbnail: imageLinks.thumbnail,
      };
    });

    switch (response.statusCode) {
      case HttpStatusCode.OK:
        return books;
      default:
        throw new UnexpectedError();
    }
  },
};
