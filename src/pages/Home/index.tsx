import React from 'react';
import Header from '../../components/Header';
import Main from '../../components/Main';
import Books from '../../components/Books';
import Footer from '../../components/Footer';

import { Container } from './styles';

const Home: React.FC = () => (
  <Container>
    <Header />
    <Main />
    <Books />
    <Footer />
  </Container>
);

export default Home;
