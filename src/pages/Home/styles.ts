import styled from 'styled-components';

export const Container = styled.div`
  background-color: ${({ theme }) => theme.colors.primary};

  @media screen and (min-width: 768px) {
    padding-top: 60px;
  }
`;
