import React from 'react';

import { Container, Content, CloseButton } from './styles';

type Props = {
  children: React.ReactElement;
  show: boolean;
  onToggle: () => void;
};

const Modal: React.FC<Props> = ({ children, show, onToggle }) => {
  return show ? (
    <Container>
      <Content>
        <CloseButton onClick={onToggle}>&#10006;</CloseButton>
        {children}
      </Content>
    </Container>
  ) : null;
};

export default React.memo(Modal);
