import styled from 'styled-components';

export const Container = styled.div`
  position: fixed;
  width: 100vw;
  height: 100vh;
  top: 0;
  left: 0;
  background-color: #777a;
  z-index: 10000;
  display: flex;
  align-items: center;
  justify-content: center;
  padding: 15px;

  @media screen and (min-width: 768px) {
    padding: 0px;
  }
`;

export const Content = styled.div`
  background-color: #fff;
  width: auto;
  max-width: 100%;
  height: auto;
  min-height: 300px;
  max-height: 100%;
  padding: 15px;
  border-radius: 5px;
  position: relative;
  overflow-y: auto;

  @media screen and (min-width: 768px) {
    min-width: 500px;
  }
`;

export const CloseButton = styled.button`
  position: absolute;
  top: 12px;
  right: 12px;
  background-color: transparent;
  border: none;
  line-height: 1;
  font-size: 18px;
  transition: all 0.3s ease;

  :hover {
    opacity: 0.7;
  }
`;
