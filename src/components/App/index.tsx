import React from 'react';
import { ThemeProvider } from 'styled-components';
import Router from '../Router';
import GlobalStyles from '../../styles/global';
import { theme } from '../../styles/theme';

const App: React.FC = () => (
  <ThemeProvider theme={theme}>
    <GlobalStyles />
    <Router />
  </ThemeProvider>
);

export default App;
