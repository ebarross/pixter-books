import styled from 'styled-components';

export const StyledInput = styled.input`
  padding: 6px 15px;
  border-radius: 5px;
  font-size: 13px;
  transition: all 0.4s ease;
  border: 1px solid ${({ theme }) => theme.colors.dark};

  :focus {
    border-color: ${({ theme }) => theme.colors.primary};
  }
`;
