import styled from 'styled-components';

export const Container = styled.div``;

export const Content = styled.div`
  display: flex;
  flex-wrap: wrap;
  margin-left: -15px;
  margin-right: -15px;

  @media screen and (min-width: 992px) {
    margin-left: -50px;
    margin-right: -50px;
  }
`;
