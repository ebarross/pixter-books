import React from 'react';
import { Container, Content } from './styles';
import { Book as TBook } from '../../../../interfaces/book';

type Props = {
  data: TBook;
  onClick: (id: string) => void;
};

const Book: React.FC<Props> = ({ data, onClick }) => {
  return (
    <Container>
      <Content onClick={() => onClick(data.id)}>
        <img src={data.thumbnail} alt="Book" />
      </Content>
    </Container>
  );
};

export default Book;
