import styled from 'styled-components';

export const Container = styled.div`
  width: 50%;
  padding: 0px 15px;
  margin-bottom: 28px;

  @media screen and (min-width: 992px) {
    width: 25%;
    padding: 0px 50px;
    margin-bottom: 48px;
  }
`;

export const Content = styled.div`
  width: 100%;
  cursor: pointer;
  transition: all 0.3s ease;

  :hover {
    transform: translateY(-5px);
  }

  img {
    width: 100%;
  }
`;
