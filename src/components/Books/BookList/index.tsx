import React from 'react';
import Book from './Book';
import { Container, Content } from './styles';
import { Book as TBook } from '../../../interfaces/book';

type Props = {
  books: TBook[];
  onBookClick: (id: string) => void;
};

const BookList: React.FC<Props> = ({ books, onBookClick }) => {
  return (
    <Container>
      <Content>
        {books.map((book) => (
          <Book key={book.id} data={book} onClick={onBookClick} />
        ))}
      </Content>
    </Container>
  );
};

export default BookList;
