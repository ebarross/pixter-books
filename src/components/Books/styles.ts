import styled from 'styled-components';

export const Container = styled.div`
  background-color: #fff;
  padding: 50px 0px;

  @media screen and (min-width: 768px) {
    padding: 80px 0px 110px;
  }
`;

export const Content = styled.div`
  width: 100%;
  padding: 0px 15px;
  margin: 0px auto;
  text-align: center;

  @media screen and (min-width: 576px) {
    max-width: 540px;
  }

  @media screen and (min-width: 768px) {
    max-width: 720px;
  }

  @media screen and (min-width: 992px) {
    max-width: 860px;
  }
`;

export const Title = styled.h1`
  font-size: 25px;
  font-weight: bold;
  margin-bottom: 28px;
`;

export const Description = styled.p`
  font-size: 13px;
  margin-bottom: 35px;
`;

export const ErrorMessage = styled.p`
  font-size: 20px;
`;
