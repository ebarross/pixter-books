import React, { useState, useEffect } from 'react';
import BookList from './BookList';
import { Container, Content, Title, Description, ErrorMessage } from './styles';
import { Book } from '../../interfaces/book';
import BookService from '../../services/books';
import BookModal from './BookModal';

const Main: React.FC = () => {
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(false);
  const [showModal, setShowModal] = useState(false);
  const [books, setBooks] = useState<Book[]>([]);
  const [selectedBook, setSelectedBook] = useState<Book | null>(null);

  useEffect(() => {
    setLoading(true);
    BookService.getAll()
      .then((response) => setBooks(response))
      .catch(() => setError(true))
      .finally(() => setLoading(false));
  }, []);

  const handleModalToggle = () => {
    setShowModal(!showModal);
  };

  const handleBookClick = (id: string): void => {
    const book = books.find((b) => b.id === id);
    setSelectedBook(book || null);
    handleModalToggle();
  };

  const renderContent = (): React.ReactElement => {
    if (loading) {
      return <p>Loading books...</p>;
    }

    let content;
    if (error || books.length === 0) {
      content = <ErrorMessage>No books were found. :(</ErrorMessage>;
    } else {
      content = <BookList books={books} onBookClick={handleBookClick} />;
    }

    return content;
  };

  return (
    <>
      <Container>
        <Content>
          <Title>Books</Title>
          <Description>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent
            vitae eros eget tellus tristique bibendum. Donec rutrum sed sem quis
            venenatis.
          </Description>
          {renderContent()}
        </Content>
      </Container>
      {selectedBook && (
        <BookModal
          data={selectedBook}
          show={showModal}
          onToggle={() => handleModalToggle()}
        />
      )}
    </>
  );
};

export default Main;
