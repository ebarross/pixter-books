import React from 'react';
import moment from 'moment';
import Modal from '../../Modal';
import { Book } from '../../../interfaces/book';

import { Container, Thumbnail, Content, Title, Section } from './styles';

type Props = {
  data: Book;
  show: boolean;
  onToggle: () => void;
};

const BookModal: React.FC<Props> = ({ data, show, onToggle }) => {
  const { title, description, thumbnail, publishedDate } = data;

  const formatDate = (): string => {
    return moment(publishedDate).format('MMMM DD, YYYY');
  };

  return (
    <Modal show={show} onToggle={onToggle}>
      <Container>
        <Thumbnail>
          <img src={thumbnail} alt="Book thumbnail" />
        </Thumbnail>
        <Content>
          <Title>{title}</Title>
          <Section>
            <p className="section-title">Description:</p>
            <p className="section-text">{description}</p>
          </Section>
          <Section>
            <p className="section-title">Published Date:</p>
            <p className="section-text">{formatDate()}</p>
          </Section>
        </Content>
      </Container>
    </Modal>
  );
};

export default BookModal;
