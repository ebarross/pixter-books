import styled from 'styled-components';

export const Container = styled.div`
  display: flex;
  align-items: center;
  flex-direction: column;

  @media screen and (min-width: 992px) {
    flex-direction: row;
    align-items: flex-start;
  }
`;

export const Thumbnail = styled.div`
  width: 180px;
  margin-bottom: 10px;

  img {
    width: 100%;
  }

  @media screen and (min-width: 992px) {
    margin-bottom: 0px;
    margin-right: 20px;
  }
`;

export const Content = styled.div`
  width: 100%;

  @media screen and (min-width: 768px) {
    width: 500px;
  }

  @media screen and (min-width: 992px) {
    width: 700px;
  }
`;

export const Title = styled.h3`
  font-size: 22px;
  margin-bottom: 8px;
`;

export const Section = styled.p`
  margin-bottom: 8px;

  .section-title {
    font-size: 15px;
    font-weight: bold;
    margin-bottom: 3px;
  }

  .section-text {
    font-size: 13px;
  }
`;
