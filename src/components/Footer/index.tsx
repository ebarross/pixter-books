import React from 'react';
import Form from './Form';
import Social from './Social';
import Addresses from './Addresses';

import { Container, Content, Title, Text } from './styles';

const Footer: React.FC = () => {
  return (
    <footer>
      <Container>
        <Content>
          <Title>Keep in touch with us</Title>
          <Text>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent
            vitae eros eget tellus tristique bibendum. Donec rutrum sed sem quis
            venenatis.
          </Text>
          <Form />
          <Social />
          <Addresses />
        </Content>
      </Container>
    </footer>
  );
};

export default Footer;
