import React, { useState } from 'react';
import Button from '../../Button';
import { Container, Content, StyledInput } from './styles';

const Form: React.FC = () => {
  const [email, setEmail] = useState('');

  const handleEmailChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setEmail(e.target.value);
  };

  const handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    // Send e-mail
  };

  return (
    <Container>
      <form onSubmit={handleSubmit}>
        <Content>
          <StyledInput
            required
            type="email"
            placeholder="enter your email to update"
            value={email}
            onChange={handleEmailChange}
          />
          <Button type="submit">Submit</Button>
        </Content>
      </form>
    </Container>
  );
};

export default Form;
