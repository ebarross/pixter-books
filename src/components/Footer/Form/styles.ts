import styled from 'styled-components';
import Input from '../../Input';

export const Container = styled.div`
  margin-bottom: 53px;
  width: 100%;
`;

export const Content = styled.div`
  display: flex;
  flex-direction: column;

  @media screen and (min-width: 992px) {
    flex-direction: row;
    justify-content: center;
  }
`;

export const StyledInput = styled(Input)`
  width: 100%;
  margin-bottom: 10px;

  @media screen and (min-width: 992px) {
    width: 450px;
    margin-bottom: 0px;
    margin-right: 20px;
  }
`;
