import React from 'react';
import { ReactComponent as Facebook } from '../../../assets/icons/facebook.svg';
import { ReactComponent as Twitter } from '../../../assets/icons/twitter.svg';
import { ReactComponent as GooglePlus } from '../../../assets/icons/google-plus.svg';
import { ReactComponent as Pinterest } from '../../../assets/icons/pinterest.svg';

import { Container, Icon } from './styles';

const Social: React.FC = () => (
  <Container>
    <Icon href="#">
      <Facebook />
    </Icon>
    <Icon href="#">
      <Twitter />
    </Icon>
    <Icon href="#">
      <GooglePlus />
    </Icon>
    <Icon href="#">
      <Pinterest />
    </Icon>
  </Container>
);

export default Social;
