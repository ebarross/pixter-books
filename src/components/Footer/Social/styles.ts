import styled from 'styled-components';

export const Container = styled.div`
  display: flex;
  margin-bottom: 63px;
`;

export const Icon = styled.a`
  width: 44px;
  height: 44px;

  :not(:first-child) {
    margin-left: 50px;
  }

  svg {
    transition: all 0.3s ease;
    opacity: 1;

    :hover {
      opacity: 0.6;
    }
  }
`;
