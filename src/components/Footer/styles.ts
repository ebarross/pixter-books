import styled from 'styled-components';

export const Container = styled.div`
  background-color: ${({ theme }) => theme.colors.dark};
  padding: 80px 0px;
`;

export const Content = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  text-align: center;

  width: 100%;
  padding: 0px 15px;
  margin: 0px auto;

  @media screen and (min-width: 576px) {
    max-width: 540px;
  }

  @media screen and (min-width: 768px) {
    max-width: 720px;
  }

  @media screen and (min-width: 992px) {
    max-width: 960px;
  }

  @media screen and (min-width: 1200px) {
    max-width: 1140px;
  }

  @media screen and (min-width: 1366px) {
    max-width: 1056px;
  }
`;

export const Title = styled.h1`
  font-weight: bold;
  font-size: 25px;
  color: ${({ theme }) => theme.colors.primary};
  margin-bottom: 28px;
`;

export const Text = styled.p`
  font-size: 13px;
  color: ${({ theme }) => theme.colors.light};
  max-width: 800px;
  margin-bottom: 50px;
`;
