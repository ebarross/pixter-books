import styled from 'styled-components';

export const Container = styled.div`
  width: 50%;
  padding: 0px 15px;
  margin-bottom: 20px;

  @media (min-width: 992px) {
    width: 150px;
    padding: 0px;
    margin-bottom: 0px;

    :not(:first-child) {
      margin-left: 78px;
    }
  }

  p {
    font-size: 13px;
    line-height: 2;
  }
`;
