import React from 'react';

import { Container } from './styles';

type Props = {
  text: string;
};

const Address: React.FC<Props> = ({ text }) => {
  return (
    <Container>
      <p>{text}</p>
    </Container>
  );
};

export default Address;
