import styled from 'styled-components';

export const Container = styled.div`
  width: 100%;
  display: flex;
  flex-wrap: wrap;
  color: #fff;
  text-align: left;
  margin-left: -30px;
  margin-right: -30px;

  @media screen and (min-width: 992px) {
    margin: 0px;
    flex-wrap: nowrap;
  }
`;
