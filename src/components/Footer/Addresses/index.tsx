import React from 'react';
import Address from './Address';

import { Container } from './styles';

const Addresses: React.FC = () => {
  return (
    <Container>
      <Address
        text="Alameda Santos, 1970 6th floor - Jardim Paulista São Paulo - SP +55 11
          3090 8500"
      />
      <Address text="London - UK 125 Kingsway London WC2B 6NH" />
      <Address text="Lisbon - Portugal Rua Rodrigues Faria, 103 4th floor Lisbon - Portugal" />
      <Address text="Curitiba – PR R. Francisco Rocha, 198 Batel – Curitiba – PR" />
      <Address text="Buenos Aires – Argentina Esmeralda 950 Buenos Aires B C1007" />
    </Container>
  );
};

export default Addresses;
