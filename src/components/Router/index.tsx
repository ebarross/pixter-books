import React from 'react';
import { BrowserRouter, Route, Redirect } from 'react-router-dom';
import Home from '../../pages/Home';

const Router: React.FC = () => (
  <BrowserRouter>
    <Route exact path="/">
      <Redirect to="/home" />
    </Route>
    <Route path="/home">
      <Home />
    </Route>
  </BrowserRouter>
);

export default Router;
