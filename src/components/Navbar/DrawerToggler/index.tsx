import React from 'react';

import { Container } from './styles';

type Props = {
  isOpen?: boolean;
  onToggle: () => void;
};

const DrawerToggler: React.FC<Props> = ({ isOpen = false, onToggle }) => {
  return (
    <Container isOpen={isOpen} onClick={onToggle}>
      <span />
      <span />
      <span />
    </Container>
  );
};

export default DrawerToggler;
