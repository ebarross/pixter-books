import styled from 'styled-components';

type Props = {
  isOpen: boolean;
};

export const Container = styled.div<Props>`
  display: flex;
  flex-direction: column;
  cursor: pointer;

  span {
    width: 30px;
    height: 2px;
    background-color: #333;
    transition: all 0.3s ease;
    border-radius: 10px;

    :not(:first-child) {
      margin-top: 8px;
    }

    :nth-child(1) {
      transform: ${({ isOpen }) =>
        isOpen ? 'translateY(10px) rotate(-45deg)' : 'none'};
    }
    :nth-child(2) {
      visibility: ${({ isOpen }) => (isOpen ? 'hidden' : 'visible')};
      opacity: ${({ isOpen }) => (isOpen ? '0' : '1')};
    }
    :nth-child(3) {
      transform: ${({ isOpen }) =>
        isOpen ? 'translateY(-10px) rotate(45deg)' : 'none'};
    }
  }

  @media screen and (min-width: 768px) {
    display: none;
  }
`;
