import React from 'react';

import { Link } from './styles';

type Props = {
  title: string;
  href: string;
};

const NavItem: React.FC<Props> = ({ title, href }) => {
  return <Link href={href}>{title}</Link>;
};

export default NavItem;
