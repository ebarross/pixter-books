import styled from 'styled-components';

export const Link = styled.a`
  font-size: 22px;
  font-weight: bold;
  color: ${({ theme }) => theme.colors.dark};

  &:not(:last-child) {
    margin-right: 49px;
  }
`;
