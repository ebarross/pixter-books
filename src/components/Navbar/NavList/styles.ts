import styled from 'styled-components';

type Props = {
  isDrawerOpen: boolean;
};

export const Container = styled.div<Props>`
  display: flex;
  flex-direction: column;
  justify-content: space-around;
  position: absolute;
  width: 100%;
  top: ${(props) => (props.isDrawerOpen ? '60px' : '-120px')};
  left: 0px;
  background-color: ${({ theme }) => theme.colors.primary};
  padding: 10px 20px;
  z-index: 999;
  box-shadow: 0 2px 3px rgb(0 0 0 / 0.2);
  transition: all 0.4s ease;

  @media screen and (min-width: 768px) {
    flex-direction: row;
    position: static;
    width: auto;
    box-shadow: none;
    padding: 0px;
  }
`;
