import React from 'react';
import NavItem from './NavItem';

import { Container } from './styles';

type Props = {
  isDrawerOpen: boolean;
};

const NavList: React.FC<Props> = ({ isDrawerOpen }) => {
  return (
    <Container isDrawerOpen={isDrawerOpen}>
      <NavItem title="Books" href="#books" />
      <NavItem title="Newsletter" href="#newsletter" />
      <NavItem title="Address" href="#address" />
    </Container>
  );
};

export default NavList;
