import React from 'react';
import Logo from '../Logo';
import NavList from './NavList';
import DrawerToggler from './DrawerToggler';

import { Container } from './styles';

type Props = {
  isDrawerOpen: boolean;
  onDrawerToggle: () => void;
};

const Navbar: React.FC<Props> = ({ isDrawerOpen, onDrawerToggle }) => {
  return (
    <Container>
      <Logo />
      <DrawerToggler isOpen={isDrawerOpen} onToggle={onDrawerToggle} />
      <NavList isDrawerOpen={isDrawerOpen} />
    </Container>
  );
};

export default Navbar;
