import styled from 'styled-components';

export const StyledButton = styled.button`
  background-color: ${({ theme }) => theme.colors.primary};
  border: 1px solid ${({ theme }) => theme.colors.primary};
  border-radius: 5px;
  padding: 6px 30px;
  font-size: 13px;
  font-weight: bold;
  text-transform: uppercase;
  color: #333333;
  transition: all 0.4s ease;

  :hover {
    background-color: rgba(252, 219, 0, 0.7);
    border-color: rgba(252, 219, 0, 0.7);
    color: ${({ theme }) => theme.colors.dark};
  }
`;
