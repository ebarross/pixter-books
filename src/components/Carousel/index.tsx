import React from 'react';
import Item from './Item';

import { Container, Content, Selector, SelectorItem } from './styles';

const Carousel: React.FC = () => {
  return (
    <Container>
      <Content>
        <Item />
      </Content>
      <Selector>
        <SelectorItem active />
        <SelectorItem />
        <SelectorItem />
      </Selector>
    </Container>
  );
};

export default Carousel;
