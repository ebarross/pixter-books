import React from 'react';
import { ReactComponent as AndroidIcon } from '../../../assets/icons/android.svg';
import { ReactComponent as AppleIcon } from '../../../assets/icons/apple.svg';
import { ReactComponent as WindowsIcon } from '../../../assets/icons/windows.svg';
import SlideImage from '../../../assets/images/book1.png';

import {
  Container,
  Infos,
  Title,
  Subtitle,
  Description,
  Image,
  Icons,
} from './styles';

const Item: React.FC = () => {
  return (
    <Container>
      <Infos>
        <Title>Pixter Digital Books</Title>
        <Subtitle>
          Lorem ipsum dolor sit amet? consectetur elit, volutpat.
        </Subtitle>
        <Description>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent
          vitae eros eget tellus tristique bibendum. Donec rutrum sed sem quis
          venenatis. Proin viverra risus a eros volutpat tempor. In quis arcu et
          eros porta lobortis sit
        </Description>
        <Icons>
          <AndroidIcon />
          <AppleIcon />
          <WindowsIcon />
        </Icons>
      </Infos>
      <Image>
        <img src={SlideImage} alt="Slide" />
      </Image>
    </Container>
  );
};

export default Item;
