import styled from 'styled-components';

export const Container = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: space-between;
  padding-bottom: 60px;

  @media screen and (min-width: 768px) {
    padding-bottom: 0px;
  }
`;

export const Infos = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  width: 100%;

  @media screen and (min-width: 768px) {
    width: 55%;
  }
`;

export const Title = styled.h1`
  font-size: 33px;
  font-weight: bold;
  line-height: 45px;
  margin-bottom: 15px;

  @media screen and (min-width: 768px) {
    margin-bottom: 36px;
  }
`;

export const Subtitle = styled.h2`
  font-weight: normal;
  font-size: 17px;
  line-height: 23px;
  margin-bottom: 15px;
  max-width: 320px;

  @media screen and (min-width: 768px) {
    margin-bottom: 36px;
  }
`;

export const Description = styled.p`
  font-weight: 300;
  font-size: 17px;
  line-height: 23px;
  margin-bottom: 30px;

  @media screen and (min-width: 768px) {
    margin-bottom: 38px;
  }
`;

export const Icons = styled.div`
  text-align: center;

  svg {
    &:not(:first-child) {
      margin-left: 38px;
    }
  }

  @media screen and (min-width: 768px) {
    text-align: left;
  }
`;

export const Image = styled.div`
  width: 80%;
  order: -1;
  margin-left: auto;
  margin-right: auto;

  img {
    width: 100%;
    margin-left: -4.5%;
    margin-top: -10px;
  }

  @media screen and (min-width: 768px) {
    width: 30%;
    order: 0;
    margin-left: 0;
    margin-right: 0;

    img {
      margin: 0;
    }
  }
`;
