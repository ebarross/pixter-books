import styled from 'styled-components';

type SelectorItemProps = {
  active?: boolean;
};

export const Container = styled.div`
  position: relative;
`;

export const Content = styled.div``;

export const Selector = styled.div`
  position: absolute;
  bottom: 0;
  left: 50%;
  transform: translateX(-50%);
  display: flex;
`;

export const SelectorItem = styled.span<SelectorItemProps>`
  width: 20px;
  height: 20px;
  border-radius: 50%;
  background-color: rgba(
    255,
    255,
    255,
    ${(props) => (props.active ? '1' : '0.7')}
  );
  cursor: pointer;

  &:not(:first-child) {
    margin-left: 17px;
  }
`;
