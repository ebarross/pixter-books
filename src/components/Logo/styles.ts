import styled from 'styled-components';

type Props = {
  width: number;
  height: number;
};

export const Container = styled.div<Props>`
  width: ${(props) => props.width}px;
  height: ${(props) => props.height}px;

  img {
    width: 100%;
  }
`;
