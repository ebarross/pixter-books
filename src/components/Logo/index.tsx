import React from 'react';

import { Container } from './styles';
import logo from '../../assets/images/logo.png';

type Props = {
  width?: number;
  height?: number;
};

const Logo: React.FC<Props> = ({ width = 149, height = 43 }) => {
  return (
    <Container width={width} height={height}>
      <img src={logo} alt="Pixter Logo" />
    </Container>
  );
};

export default Logo;
