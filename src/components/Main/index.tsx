import React from 'react';
import Carousel from '../Carousel';

import { Container, Content } from './styles';

const Main: React.FC = () => {
  return (
    <Container>
      <Content>
        <Carousel />
      </Content>
    </Container>
  );
};

export default Main;
