import styled from 'styled-components';

export const Container = styled.div`
  width: 100%;
  min-height: calc(100vh - 70px - 60px);
  background-color: ${({ theme }) => theme.colors.primary};
  padding-bottom: 30px;

  @media screen and (min-width: 768px) {
    padding-top: 50px;
  }
`;

export const Content = styled.div`
  width: 100%;
  padding: 0px 15px;
  margin: 0px auto;

  @media screen and (min-width: 576px) {
    max-width: 540px;
  }

  @media screen and (min-width: 768px) {
    max-width: 720px;
  }

  @media screen and (min-width: 992px) {
    max-width: 960px;
  }

  @media screen and (min-width: 1200px) {
    max-width: 1140px;
  }

  @media screen and (min-width: 1366px) {
    max-width: 1200px;
  }
`;
