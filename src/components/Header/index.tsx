import React, { useState } from 'react';
import Navbar from '../Navbar';

import { Container, Content } from './styles';

const Header: React.FC = () => {
  const [showDrawer, setShowDrawer] = useState(false);

  const handleDrawerToggle = () => {
    setShowDrawer(!showDrawer);
  };

  return (
    <header>
      <Container>
        <Content>
          <Navbar
            isDrawerOpen={showDrawer}
            onDrawerToggle={handleDrawerToggle}
          />
        </Content>
      </Container>
    </header>
  );
};

export default Header;
