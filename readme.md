## About The Project

This is a basic landing page for the Pixter Books project.

### Build With

- React
- TypeScript
- Styled Components
- Axios
- ESLint + Prettier

## Getting Started

This is an example of how you can run the project locally.

### Installation

1.  Clone the repo

```sh
git clone https://bitbucket.org/ebarross/pixter-books.git
```

2.  Install NPM packages

```sh
npm install
```

### Running

```sh
npm start
```
